import time
start_time = time.time()

def sum_coins(coins_list):
    numbers_of_combinations = 0
    combination = [0,0,0,0,0,0,0]
    i = 0
    while combination[6] != 200:
        combination[i] += 1
        if sum([a * b for a, b in zip(combination, coins_list)]) == 200:
            numbers_of_combinations += 1
            for j in range(i+1, 0, -1):
                if i == 6:
                    j -= 1
                    i -= 1
                if combination[j-1] != 0:
                    combination[j-1] -= 1
                    combination[6] = 0
                    i = j 
                    break
        elif sum([a * b for a, b in zip(combination, coins_list)]) > 200:
            combination[i] -= 1
            i+=1
    return numbers_of_combinations

print(sum_coins([100,50,20,10,5,2,1]) + 1) #We add 1 for the case where we just take a coin of 2 pounds
print(f"--- {(time.time() - start_time):.10f} seconds ---"  )