#This solution is not mine but I keep it here because it's beautiful and super efficient
import time
start_time = time.time()

def w(target, coins):
    sols = 0

    if target % coins[0] == 0:
        sols += 1

    if len(coins) > 1:
        while target > 0:
            sols += w(target, coins[1:]) 
            target -= coins[0]

    return sols

print(w(200,[200,100,50,20,10,5,2,1]))
print(f"--- {(time.time() - start_time):.10f} seconds ---"  )